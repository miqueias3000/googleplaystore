
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType
import org.scalatest.FunSpec

import com.github.mrpowers.spark.fast.tests.DataFrameComparer
import com.miqueias.googleplaystore.SparkSessionTest
import com.miqueias.googleplaystore.domain.playstore.PlayStoreApp

class PlayStoreAppSpec extends FunSpec
    with DataFrameComparer
    with SparkSessionTest {

    it("It should return 'Apps' with 'Rating' greater or equal than four, and sorted descending") {
        // Given
        val df = givenMocks()

        // When
        val dfAvgSentimentPolarity = new PlayStoreApp().appsWithRatingGreaterOrEqualThanFour(df)
        val expectedResult = givenExpectedMockResult()

        // Then
        assertSmallDataFrameEquality(dfAvgSentimentPolarity, expectedResult)
    }

    def givenMocks(): Dataset[Row] = {

        val userReviews = Seq(
            Row("10 Best Foods for You", "3.1", "ART_AND_DESIGN"),
            Row("Photo Editor", "4.3", "ART_AND_DESIGN"),
            Row("Coloring book", "4.25", "ART_AND_DESIGN"),
            Row("Pixel Draw", "5.0", "ART_AND_DESIGN"),
            Row("Sketch", "3.9", "ART_AND_DESIGN"),
            Row("Clash of Clans", "4.6", "GAME"),
            Row("Clash of Clans", "4.6", "FAMILY"))

        val schema = new StructType()
            .add("App", StringType)
            .add("Rating", DoubleType)
            .add("Category", StringType, true)

        spark.createDataFrame(spark.sparkContext.parallelize(userReviews), schema)
    }

    def givenExpectedMockResult(): Dataset[Row] = {
        val expectedSchema = List(
            StructField("App", StringType, false),
            StructField("Rating", DoubleType, false),
            StructField("Category", StringType, false))

        val expectedDf = Seq(
            Row("Pixel Draw", "5.0", "ART_AND_DESIGN"),
            Row("Clash of Clans", "4.6", "GAME"),
            Row("Clash of Clans", "4.6", "FAMILY"),
            Row("Photo Editor", "4.3", "ART_AND_DESIGN"),
            Row("Coloring book", "4.25", "ART_AND_DESIGN"))

        spark.createDataFrame(
            spark.sparkContext.parallelize(expectedDf),
            StructType(expectedSchema))
    }
}
