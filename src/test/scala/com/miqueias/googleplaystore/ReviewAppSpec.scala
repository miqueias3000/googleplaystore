
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType
import org.scalatest.FunSpec

import com.github.mrpowers.spark.fast.tests.DataFrameComparer
import com.miqueias.googleplaystore.SparkSessionTest
import com.miqueias.googleplaystore.domain.review.ReviewApp

class ReviewAppSpec extends FunSpec
    with DataFrameComparer
    with SparkSessionTest {

    it("Get the right average of 'Sentiment Polarity', grouped by 'App' column") {
        // Given
        val df = givenMocks()

        // When
        val dfAvgSentimentPolarity = new ReviewApp().averageSentimentPolarityGroupedByAppName(df)
        val expectedResult = givenExpectedMockResult()

        // Then
        assertSmallDataFrameEquality(dfAvgSentimentPolarity, expectedResult)
    }

    def givenMocks(): Dataset[Row] = {

        val userReviews = Seq(
            Row("10 Best Foods for You", "0.15", "0.777"),
            Row("10 Best Foods for You", "0.05", "0.321"),
            Row("10 Best Foods for You", "0.35", "0.528923"),
            Row("10 Best Foods for You", "0.25", "0.2111"),
            Row("Sketch", "0.25", "0.9876"),
            Row("Sketch", "0.25", "0.1234"))

        val schema = new StructType()
            .add("App", StringType)
            .add("Sentiment_Polarity", DoubleType)
            .add("Sentiment_Subjectivity", DoubleType, true)

        spark.createDataFrame(spark.sparkContext.parallelize(userReviews), schema)
    }

    def givenExpectedMockResult(): Dataset[Row] = {
        val expectedSchema = List(
            StructField("App", StringType, true),
            StructField("Average_Sentiment_Polarity", DoubleType, false))

        val expectedAvgDf = Seq(
            Row("10 Best Foods for You", "0.2"),
            Row("Sketch", "0.25"))

        spark.createDataFrame(
            spark.sparkContext.parallelize(expectedAvgDf),
            StructType(expectedSchema))
    }
}
