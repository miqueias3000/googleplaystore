package com.miqueias.googleplaystore

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession

import com.miqueias.googleplaystore.domain.playstore.PlayStoreApp
import com.miqueias.googleplaystore.domain.review.ReviewApp
import com.miqueias.googleplaystore.infra.CsvReader
import com.miqueias.googleplaystore.infra.CsvSerializer

object GooglePlayStoreApplication {

    val spark = SparkSession.builder.appName("Google Play Store Application").master("local").getOrCreate()

    def main(args: Array[String]): Unit = {

        val playStoreApp = new PlayStoreApp
        val reviewApp = new ReviewApp

        val csvReader = new CsvReader(spark.read)
        val rawPlayStoreFileDataFrame = csvReader.readFile("googleplaystore.csv", playStoreApp.schema())
        val rawReviewsFileDataFrame = csvReader.readFile("googleplaystore_user_reviews.csv", reviewApp.schema())

        val playStoreDataFrame = removeDuplicatedLines(rawPlayStoreFileDataFrame)
        val reviewsDataFrame = removeDuplicatedLines(rawReviewsFileDataFrame)

        val csvSerialize = new CsvSerializer

        //# Part 1
        val dfAvgSentimentPolarity = reviewApp.averageSentimentPolarityGroupedByAppName(reviewsDataFrame)
        csvSerialize.serialize("average-sentiment-polarity-by-app", dfAvgSentimentPolarity)

        //# Part 2
        val appsGreaterEqualThenFour = playStoreApp.appsWithRatingGreaterOrEqualThanFour(playStoreDataFrame)
        csvSerialize.serialize("apps-greater-equal-four-sorted-descending", appsGreaterEqualThenFour, "§")

        //# Part 3
        val appsUniqueWithGroupedCategoryAndMaximumNumberOfReviews = playStoreApp.appsUniqueWithGroupedCategoryAndMaximumNumberOfReviews(playStoreDataFrame)
        csvSerialize.serialize("apps-unique-grouped-category-maximum-number-of-reviews", appsUniqueWithGroupedCategoryAndMaximumNumberOfReviews)

        spark.stop()
    }

    def removeDuplicatedLines(dataset: Dataset[Row]): Dataset[Row] = {
        dataset.dropDuplicates()
    }
}
