package com.miqueias.googleplaystore.domain

import org.apache.spark.sql.types.StructType

trait Schemable {
    def schema(): StructType
}
