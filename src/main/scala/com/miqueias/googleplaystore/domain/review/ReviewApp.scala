package com.miqueias.googleplaystore.domain.review

import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructType
import com.miqueias.googleplaystore.domain.Schemable
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions.avg
import org.apache.spark.sql.functions._

class ReviewApp extends Schemable {
    def schema(): StructType = {
        new StructType()
            .add("App", StringType, false)
            .add("Translated_Review", StringType, true)
            .add("Sentiment", StringType, true)
            .add("Sentiment_Polarity", DoubleType, true)
            .add("Sentiment_Subjectivity", DoubleType, true)
    }

    def averageSentimentPolarityGroupedByAppName(dataFrame: Dataset[Row]): Dataset[Row] = {
        val sentimentalPolarity = col("Sentiment_Polarity")

        var sentimentalPolarityNotNullAndNotNaN = sentimentalPolarity.isNotNull
            .and(not(isnan(sentimentalPolarity)))

        dataFrame
            .filter(sentimentalPolarityNotNullAndNotNaN)
            .groupBy(col("App"))
            .agg(avg(sentimentalPolarity).as("Average_Sentiment_Polarity"))
    }
}
