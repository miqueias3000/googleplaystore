package com.miqueias.googleplaystore.domain.playstore

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.collect_list
import org.apache.spark.sql.functions.concat
import org.apache.spark.sql.functions.isnan
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.max
import org.apache.spark.sql.functions.not
import org.apache.spark.sql.functions.round
import org.apache.spark.sql.functions.split
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.functions.{ when, _ }
import com.miqueias.googleplaystore.domain.Schemable

class PlayStoreApp extends Schemable {
    def schema(): StructType = {
        new StructType()
            .add("App", StringType, false)
            .add("Category", StringType, true)
            .add("Rating", DoubleType, true)
            .add("Reviews", IntegerType, true)
            .add("Size", StringType, true)
            .add("Installs", StringType, true)
            .add("Type", StringType, true)
            .add("Price", DoubleType, true)
            .add("Content Rating", StringType, true)
            .add("Genres", StringType, true)
            .add("Last Updated", StringType, true)
            .add("Current Ver", StringType, true)
            .add("Android Ver", StringType, true)
    }

    def appsWithRatingGreaterOrEqualThanFour(dataFrame: Dataset[Row]): Dataset[Row] = {
        val rating = col("Rating")

        var ratingNotNullNotNaNEqualOrGreaterThenFour = rating.isNotNull
            .and(not(isnan(rating)))
            .and(rating.geq(4.0))

        dataFrame
            .filter(ratingNotNullNotNaNEqualOrGreaterThenFour)
            .sort(rating.desc)
    }

    def appsUniqueWithGroupedCategoryAndMaximumNumberOfReviews(dataFrame: Dataset[Row]): Dataset[Row] = {
        val size = col("Size")
        val reviews = col("Reviews")
        val categoria = col("Category")
        val sizeTranformation = concat(round(split(size, "k").getItem(0).cast(IntegerType) / 1024, 2), lit("M"))

        val dfWithouSomeCol = dataFrame
            .select(col("App"), col("Rating"),
                col("Installs"), col("Type"),
                col("Price"), col("Content Rating"),
                col("Last Updated"), col("Current Ver"), col("Android Ver"),
                when(size.endsWith("k"), sizeTranformation)
                    .otherwise(size).as("Size"))

        val dfAgg = dataFrame
            .groupBy("App")
            .agg(
                collect_list(categoria).cast("string").as("Categories"),
                collect_list("Genres").cast("string").as("Genres"),
                max(reviews))
            .drop("Category")

        dfWithouSomeCol.join(dfAgg, "App").dropDuplicates()
    }
}
