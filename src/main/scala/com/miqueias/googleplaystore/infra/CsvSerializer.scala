package com.miqueias.googleplaystore.infra

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.SaveMode

class CsvSerializer extends FileSerializable {

    def serialize(path: String, dataFrame: Dataset[Row], delimiter: String = ",") {
        dataFrame
            .repartition(1)
            .write
            .mode(SaveMode.Overwrite)
            .option("header", "true")
            .option("delimiter", delimiter)
            .csv(s"/tmp/spark/$path")
    }
}
