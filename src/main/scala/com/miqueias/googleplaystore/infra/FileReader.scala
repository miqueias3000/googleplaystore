package com.miqueias.googleplaystore.infra

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.StructType

trait FileReader {
    def readFile(fileName: String, schema: StructType): Dataset[Row]
}
