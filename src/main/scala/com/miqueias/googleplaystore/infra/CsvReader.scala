package com.miqueias.googleplaystore.infra

import scala.collection.immutable.HashMap

import org.apache.spark.sql.DataFrameReader
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.StructType

class CsvReader(dataFrameReader: DataFrameReader) extends FileReader {

    val optionsMap = HashMap("header" -> "true", "delimiter" -> ",")
    def readFile(fileName: String, schema: StructType): Dataset[Row] = {
        dataFrameReader.options(optionsMap).schema(schema).csv(s"src/main/resources/google-play-store-apps/$fileName")
    }
}
