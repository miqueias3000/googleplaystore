package com.miqueias.googleplaystore.infra

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row

trait FileSerializable {
    def serialize(path: String, dataFrame: Dataset[Row], delimiter: String)
}
