# Google PlayStore Challenge

## The Challenge
the challenge description is in the following link:

https://github.com/bdu-xpand-it/BDU-Recruitment-Challenges/wiki/Spark-2-Recruitment-Challenge

## Prerequisites
This project was developed with the following tools:

* Java 8
* Maven 3.6.3
* Scala
* Spark

## Running
How to execute this application:

## Exported Files
All the exported files will be inside "/tmp/spark" folder